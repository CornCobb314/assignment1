public class Entry {
	public String ownerDepartment;
	public Listing externalLink;
	public String account;
	public String type;
	public String incomingPercentage;
	public String incomingActivity;
	public String totalActivity;
	public String outgoingActivity;
	
	class Listing{
		public String url;
	}
	public String toString(){
		StringBuilder s = new StringBuilder();
		if (ownerDepartment != null){
			s.append(ownerDepartment);
			s.append(" ");
		}
		if (externalLink != null){
			s.append(externalLink.url);
			s.append(" ");
		}
		if (account != null){
			s.append(account);
			s.append(" ");
		}
		if (type != null){
			s.append(type);
			s.append(" ");
		}
		if (incomingPercentage != null){
			s.append(incomingPercentage);
			s.append(" ");
		}
		if (incomingActivity != null){
			s.append(incomingActivity);
			s.append(" ");
		}
		if (totalActivity != null){
			s.append(totalActivity);
			s.append(" ");
		}
		if (outgoingActivity != null){
			s.append(outgoingActivity);
			s.append("\n\n\n");
		}
		return s.toString();
	}
}