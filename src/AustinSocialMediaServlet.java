import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;


/**
 * Servlet implementation class AustinSocialMediaServlet
 */
@WebServlet("/AustinSocialMediaServlet")
public class AustinSocialMediaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AustinSocialMediaServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Get JSON
		final Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).create();
		JsonParser parser = new JsonParser();
		JsonArray jArray;
		

        Cookie cookies[] = request.getCookies() != null ? request.getCookies() : new Cookie[0];
		
		String urlString = "https://www.cs.utexas.edu/~devdatta/ej42-f7za.json/";
		URL url = new URL(urlString);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.connect();
		
		BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = br.readLine()) != null) {
            sb.append(line);
            sb.append("\n");
        }
        br.close();
        ArrayList<Entry> entries = new ArrayList<Entry>();
        jArray = parser.parse(sb.toString()).getAsJsonArray();
        for(JsonElement obj : jArray ){
            Entry entry = gson.fromJson(obj, Entry.class);
            if (entry.externalLink != null)//If there is no external link don't add it.
            	entries.add(entry);
        }
        
        //check cookies
        if (request.getParameter("username") != null){
        	boolean cookieExists = cookies.length != 0;
        	if (request.getParameter("session").toLowerCase().equals("start")){
        		if (!cookieExists){
	        		Cookie cookie = new Cookie(request.getParameter("username"),"");
	                cookie.setDomain("localhost");
	                cookie.setPath(request.getServletPath());
	                cookie.setMaxAge(1000);
	                response.addCookie(cookie);
        		}
        	}
        	else if (request.getParameter("session").toLowerCase().equals("end")){
        		if (cookieExists){
        			cookies[0].setMaxAge(0);
        		}
        		else {
        			response.getWriter().println("Disallowed session value specified for parameter session");
        		}
        	}
        	else {
        		response.getWriter().println("Disallowed session value specified for parameter session");
        	}
        }
        
        if (cookies.length != 0){
        	response.getWriter().print(cookies[0].getValue());
        }
        
        if (request.getParameter("account_name") != null){
        	if (cookies.length != 0){
	        	cookies[0].setValue(cookies[0].getValue() + "http://localhost:8080/assignment1/austinsocialmedia?account_name=" +
						request.getParameter("account_name") + "&type=" + request.getParameter("type") + "\n");
        	}
        	int urlsPrinted = 0;
        	int encounters = 0;
        	for (Entry entry : entries){
        		if (entry.account.equals(request.getParameter("account_name"))){
        			encounters++;
        			if (request.getParameter("type") == null){
        				if (urlsPrinted == 0){
        					response.getWriter().println("URL Data");
        				}
        				response.getWriter().println(entry.account + ' ' + entry.type + ' ' + entry.externalLink.url);
        				urlsPrinted++;
        			}
        			else if (request.getParameter("type").equals(entry.type)){
        				if (urlsPrinted == 0){
        					response.getWriter().println("URL Data");
        				}
        				response.getWriter().println(entry.account + ' ' + entry.type + ' ' + entry.externalLink.url);
        				urlsPrinted++;
        			}
        		}
        	}
        	if (urlsPrinted == 0){
        		if (encounters == 0){
        			response.getWriter().println("Disallowed session value specified for parameter account_name");
        		}
        		else {
        			response.getWriter().println("Disallowed session value specified for parameter type");
        		}
        	}
        }
	}

}
